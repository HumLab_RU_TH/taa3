# TAA3

This project aims to develop an automated geospatial analysis pipeline that is able to transform election data from poll stations (Provincial Elections 2019) in order to show the results on four CBS administrative levels: Buurt, Wijk, Gemeente en Provincies. 

<b>Documentation of the pipeline</b>

First the steps are explained that need to be taken before the Python script can be executed. Second the general workings of the script will be discussed. In the third part I will discuss the design and implementation choices and in the fourth and last part the advantages and disadvantages of the geospatial analysis method and visualization that I chose.


<i>Actions to undertake before the script can be executed</i>

1. Download the GeoJSON file <code>Ps2019-xygeo.json</code> (from: https://data.openstate.eu/dataset/e143d70c-8ba1-4af8-8e8c-920f176c5767/resource/adb96395-009c-46c5-b6fc-982e4c0b3c07/download/ps2019-xy.geo.json) and save it to a preferred folder.

2. Download the three CBS shapefiles (from: https://www.cbs.nl/-/media/cbs/dossiers/nederland-regionaal/wijk-en-buurtstatistieken/wijkbuurtkaart_2019_v2up.zip), unzip the file and place the three shapefiles into the same folder:
- <code>Buurt_2019_CBS.shp</code>
- <code>Wijk_2019_CBS.shp</code>
- <code>Gemeente_2019_CBS.shp</code>

3. Download the <code>Gemeenten alfabetisch 2019.xls</code> file (from: https://www.cbs.nl/-/media/cbs/onze-diensten/methoden/classificaties/overig/gemeenten-alfabetisch-2019.xls) and place the Excel-file in the same folder as the other datasets.

4. After all this is done copy the path to that folder and swap the current paths in the <code>TAA3.py</code> script with the path to the folder with the newly added (geo)data.

5. Make sure these Python packages have been installed (in your virtual environment):
- json
- geojson
- pyshp
- shapely
- pandas

6. When the packages are installed, the <code>TAA3.py</code> script can be executed.


<b>Explanation of the script</b>

In the first part of the script the shapefiles are imported, the names of each administrative unit are stored in a list and the shapes are transformed into Shapely polygons. With the help of the information in the "WATER' fields from the <code> Gemeente_2019_CBS.shp </code> shapefile only the polygons for which counts WATER == NEE (means "NO" in Dutch) are kept, together with the names of these administrative units. The last step of the first part of the script is creating province polygons using the Shapely municipality polygons and the CBS spreadsheet that links each municipality to a specific province. 

The second part of the script is the import of the GeoJSON file and the processing of the locations of the poll stations into Shapely points and the results for each of the 37 political parties from each poll station. 

The third and last part of the pipeline consists of four processes, for every administrative unit, that seeks out in which administrative unit which poll stations lie, collects the votes for each of the 37 political parties and the valid votes from these poll stations and subsequently calculates the percentages of the votes for each political party within each administrative unit. The last part of each of the four processes is the creation of 4 GeoJSON files, for each of the four administrative units, in order to store the geometries (of the administrative units) and the properties for each geometry: an index number, the name of the administrative unit, the number of polls within the unit and the percentages of votes for the 37 parties. 

This results in 4 GeoJSON files:

- <code>Neighbourhood_Election_2019.json</code>
- <code>District_Election_2019.json</code>
- <code>Municipality_Election_2019.json</code>
- <code>Province_Election_2019.json</code>


<b>Design and implementations choices</b>

Since I used PySHP and Shapely for the TAA2 assignment I gathered it would make sense to used the same packages for this assignment.

In the first version of the pipeline the user had to clean-up the shapefiles in order to create a recognizable map of the Netherlands. This meant the deletion of the polygons that symbolized the water parts (and Belgian territories within the Netherlands) in the three shapefiles in ArcMAP or QGIS before the shapefiles could be used in the Python-part of the pipeline. I also created a shapefile for the provinces using the modified version of the CBS Gemeente/Municipality shapefile and the <code> Gemeenten alfabetisch 2019</code> spreadsheet. I joined both datasets and merged all municipalities from each province in order to create the 12 Dutch provinces, added the names of the provinces to the attribute table (and deleting the remaining 12 names of municipalities within each province).

When the script was working it was clear that it was not so difficult to skip the use of an external GIS and do everything in the Python part of the pipeline and I changed the pipeline in Part 1 of the script.

I think the handling of GeoJSON could have been done more efficiently with less code and without adding the names of the political parties but when I started with this assignment I thought this way made sense and I thought it was clear what happens where with the data from the GeoJSON file and I did not know how to do it otherwise. Since I was already running behind schedule I kept it this way.

In order to check if the pipeline worked properly I decided to count the number of polls within each polygon/unit when I started with the provinces and I decided to store the polls in GeoJSON file in order to check if the process of seeking out which polls lie in which unit was done in the right way. The same goes for the names of the units but I thought it also made sense to add the name of each administrative unit while you are in the process of calculating the percentages of votes for each administrative unit and adding them to each municipality, especially for the provinces, municipalities and the districts. This way the user of the future webapplication is able to see the name of every unit he decides to inspect. 

Part 3, the part of the pipeline that seeks out which poll stations lie in which administrative unit and that combines all votes for each party per unit and calculates the percentages using the combined valid votes for each unit is executed 4 times for each of the four administrative units (Neighbourhoods, Districts, Municipalities and Provinces). If I had invested more time the four administrative units could have been combined in an extra loop and that would have reduced the repetition quite susbstantially but I am not sure that would have made things easier to understand. The storing of the results for each administrative unit in a GeoJSON file would anyhow have been executed four times.

Some administrative units (Neighbourhoods, Districts and Municipalities) do not have a polling station with its borders. The calculation of the percentages of the votes for each party is skipped in these instances since there are no valid votes to divide with. 


<b>Advantages and disadvantages of the chosen geospatial analysis and visualization </b>

<i>Geospatial analysis</i>

I used the Shapely package to create the Provincial administrative unit polygons. To be more precise, the <code>unary_union</code> method that merges polygons (Ref 1). Before intersecting the Shapely province polygons with the locations of the poll stations I stored only the 12 provinces and names in a GeoJSON had checked the results in QGIS. Since I could not detect any holes or slivers I accepted the result and moved on but I saw online that is quite possible that the polygons would have holes or slivers with the province or in between provinces (because of floating point coordinates) and that there methods to deal with them.

I chose to use the Shapely package used in the TAA2 assignment since I expected to learn more new skills than with using the Geopandas package. The <code>intersects</code> method seemed to work properly when starting with the Provincial administrative division. It "Returns True if the boundary or interior of the object intersect in any way with those of the other" according to the <i>Shapely User Manual</i> (Ref 2). Since the coordinates of the locations of the poll stations are very precise I did not think that it could be possible that point could not be completely within a polygon representing an administrative unit. Only later, when writing this essay, and reflecting on this method and I gathered that with the chosen method it could theoretically be possible to have one polling station on the border between two polygons (two administrative units) and that the results of this polling station could have been counted twice, for each administrative unit that shared this specific border. By choosing another Shapely method, the <code>within</code>method this theoretical possibility could have been avoided since it 
"Returns True if the object’s boundary and interior intersect only with the interior of the other (not its boundary or exterior)"(Ref 3). To be sure that the pipeline did not use poll stations twice, I changed my method from <i>intersects</i> to <i>within</i> before finishing this assignment. 


<i>Visualization</i>

I chose to store the data in de GeoJSON file since I had already figured out how to do this for the TAA2 assignment. I also knew that it should not be a problem to store the long names of some political parties, since they were already in the GeoJSON file with the polling stations, and this is a problem when storing the data in a shapefile. The number of characters of a fieldname of the attribute table of a shapefile is not able to store the often long names of the political parties. This would mean creating abbreviated names, more lines of code, and when using the end result in the webapplication, it would not look very nice when a user would not see the percentages paired with easily recognizable names of the political parties, when clicking on a administrative unit. 

A possible downside of the choice for a GeoJSON file is that the four GeoJSON files are quite large in comparison to the original shapefiles. This may be a problem for the responsiveness of the future webapplication. If so, I will seek out how to minimize the volume of the GeoJSON files. 

<b>References</b>
- Ref 1: https://shapely.readthedocs.io/en/stable/manual.html#object.intersects
- Ref 2: https://shapely.readthedocs.io/en/stable/manual.html#object.within
- Ref 3: https://shapely.readthedocs.io/en/stable/manual.html#shapely.ops.unary_union



