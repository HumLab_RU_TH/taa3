import pandas as pd
import shapefile
import geojson
import json
import shapely, shapely.geometry
from shapely.geometry import shape
from shapely.ops import unary_union
from geojson import FeatureCollection
from copy import deepcopy

# PART 1 
# Import and processing of the CBS shapefiles of the administrative units, 
# creation of province polygons.

# Read the shapefiles with the three CBS administrative levels with PySHP

# --> Please change paths to the correct folder on your drive <---

# Read CBS Buurten/Neighbourhoods 2019 shapefile
nbhd_shapes = shapefile.Reader("C:/unigis/internet_gis/TAA3/data/Buurt_2019_CBS.shp")

# Read CBS Wijken/Disctricts 2019 shapefile
dist_shapes = shapefile.Reader("C:/unigis/internet_gis/TAA3/data/Wijk_2019_CBS.shp")

# Read CBS Gemeenten/Municipalities 2019 shapefile
mncp_shapes = shapefile.Reader("C:/unigis/internet_gis/TAA3/data/Gemeente_2019_CBS.shp")


# Store the name and water field of the 4 administrative levels in lists

# Buurten/Neighbourhoods(nbhd)

nbhd_records = nbhd_shapes.records()
nbhd_names_temp = [row[1] for row in nbhd_records]
nbhd_water = [row[5] for row in nbhd_records] 

# Wijken/Districts(dist)

dist_records = dist_shapes.records()
dist_names_temp = [row[1] for row in dist_records]
dist_water= [row[4] for row in dist_records]

# Gemeenten/Municipalities(mncp)

mncp_records = mncp_shapes.records()
mncp_names_temp = [row[1] for row in mncp_records]
mncp_water= [row[2] for row in mncp_records]

# Store also the GM code field in a list. 
# These codes are needed to create the province polygons 
mncp_code_temp=[row[0] for row in mncp_records]


# Extract the polygons from the imported shapefiles that represent 
# land mass and convert them to Shapely polygons. 

# Neighbourhoods

# Create an empty list to store ALL the polygons from the
# CBS shapefiles
neighbourhood_list_temp=[]

# Extract all polygons from mncp_shapes
for shapy in nbhd_shapes.shapeRecords():
   feature = shapy.shape.__geo_interface__
   
   # Conversion to Shapely 
   shp_geom= shape(feature)
   
   # Store in temp neighbourhood list
   neighbourhood_list_temp.append(shp_geom)

# Create empty lists to store the selected
# landmass polygons and corresponding names
neighbourhood_list=[]
nbhd_names=[]

# Loop over the names, shapes and "water" polygons
for name, shape_nbhd, water in zip(nbhd_names_temp, neighbourhood_list_temp, nbhd_water):
    # Select only the shapes and names that are NOT marked as water
    if water == 'NEE':
        # Add names and shapes to the new lists
        nbhd_names.append(name)
        neighbourhood_list.append(shape_nbhd)         
      
        
# Districts

# Create an empty list to store ALL the polygons from the
# CBS shapefiles
district_list_temp=[]

# Extract all polygons from mncp_shapes
for shapy in dist_shapes.shapeRecords():
   feature = shapy.shape.__geo_interface__
   
   # Conversion to Shapely 
   shp_geom = shape(feature)
   
   # Store in temp district list
   district_list_temp.append(shp_geom)

# Create empty lists to store the selected
# landmass polygons and corresponding names
district_list=[]
dist_names=[]

# Loop over the names, shapes and "water" polygons
for name, shape_dist, water in zip(dist_names_temp, district_list_temp, dist_water):
    # Select only the shapes and names that are NOT marked as water
    if water == 'NEE':
        # Add names and shapes to the new lists
        dist_names.append(name)
        district_list.append(shape_dist)         
   

# Municipalities

# First create an empty list to store ALL the polygons from the
# CBS shapefiles
municipality_list_temp=[]

# Extract all polygons from mncp_shapes
for shapy in mncp_shapes.shapeRecords():
   feature = shapy.shape.__geo_interface__
   
   # Conversion to Shapely 
   shp_geom= shape(feature)
   
   # Store in temp municipality list
   municipality_list_temp.append(shp_geom)

# Create empty lists to store the selected
# landmass polygons, corresponding names and codes
municipality_list=[]
mncp_names=[]
mncp_codes=[]

# Loop over the names, shapes and "water" polygons
for name, code, shape_mncp, water in zip(mncp_names_temp, mncp_code_temp, municipality_list_temp, mncp_water):
    # Select only the shapes, names and codes that are NOT marked as water
    if water == 'NEE':
        # Add names, shapes and codes to the new lists
        mncp_names.append(name)
        municipality_list.append(shape_mncp)  
        mncp_codes.append(code)
   
    
# Create polygons for the provinces (based on municipality polygons)

# Read the CBS Excelfile Gemeenten alfabetisch 2019.xls using Pandas
# This spreadsheet contains the the names and codes of all Dutch municipalities
# and the names of the provinces in which these municipalities are located.

# --> Please change paths to correct folder on your drive <---

df = pd.read_excel("C:/unigis/internet_gis/TAA3/data/Gemeenten alfabetisch 2019.xls", sheet_name=0)

# Create two lists with the codes and provincenames of each municipality
cbs_gmcode = list(df['GemeentecodeGM']) 
cbs_provname = list(df['Provincienaam'])

# Make a dictionary that can be used to find which municipality belongs to which province
cbs_gm_prov = dict(zip(cbs_gmcode, cbs_provname))

# Create 12 lists, for each province, that will temporarily hold
# the shapes of the municipalities within each province

groningen=[]
friesland=[]
drenthe=[]
overijssel=[]
gelderland=[]
utrecht=[]
noord_holland=[]
zuid_holland=[]
zeeland=[]
noord_brabant=[]
limburg=[]
flevoland=[]

# Find out which municipality belongs to which province and
# store the corresponding shapes in the correct list
for name, code, shape_mncp in zip(mncp_names, mncp_codes, municipality_list):
    if cbs_gm_prov[code] == "Groningen":      
        groningen.append(shape_mncp)
        
    elif cbs_gm_prov[code] == "Friesland":
        friesland.append(shape_mncp)
       
    elif cbs_gm_prov[code] =="Drenthe": 
        drenthe.append(shape_mncp)
        
    elif cbs_gm_prov[code] =="Overijssel":        
        overijssel.append(shape_mncp)
        
    elif cbs_gm_prov[code] =="Gelderland":        
        gelderland.append(shape_mncp)
        
    elif cbs_gm_prov[code] =="Utrecht":      
        utrecht.append(shape_mncp)
        
    elif cbs_gm_prov[code] =="Noord-Holland":
        noord_holland.append(shape_mncp)
        
    elif cbs_gm_prov[code] =="Zuid-Holland":         
        zuid_holland.append(shape_mncp)
        
    elif cbs_gm_prov[code] =="Zeeland":       
        zeeland.append(shape_mncp)
        
    elif cbs_gm_prov[code] =="Noord-Brabant":        
        noord_brabant.append(shape_mncp)
        
    elif cbs_gm_prov[code] =="Limburg":
        limburg.append(shape_mncp)
        
    elif cbs_gm_prov[code] =="Flevoland": 
        flevoland.append(shape_mncp)
        

# Create a list to store all 12 merged province polygons later on
province_list=[]

# Merge all polygons within each list and
# add them to the new list
gr=unary_union(groningen)
province_list.append(gr)

fr=unary_union(friesland) 
province_list.append(fr) 

dr=unary_union(drenthe) 
province_list.append(dr)

ov=unary_union(overijssel) 
province_list.append(ov)

ge=unary_union(gelderland) 
province_list.append(ge)

ut=unary_union(utrecht) 
province_list.append(ut)

nh=unary_union(noord_holland) 
province_list.append(nh)

zh=unary_union(zuid_holland) 
province_list.append(zh)

ze=unary_union(zeeland) 
province_list.append(ze)

nb=unary_union(noord_brabant) 
province_list.append(nb)

lb=unary_union(limburg) 
province_list.append(lb)

fl=unary_union(flevoland) 
province_list.append(fl)

# Create list of names of provinces
prov_names=['Groningen', 'Friesland', 'Drenthe', 'Overijssel', 'Gelderland', 'Utrecht', 'Noord-Holland', 'Zuid-Holland', 'Zeeland', 'Noord-Brabant', 'Limburg', 'Flevoland']

# PART 2 
# Import and processing of the GeoJSON file with 
# locations of poll stations and election results

# Read Election Results 2019 GeoJSON file

#--> Please change paths to correct folder on your drive <---

# Open GeoJSON file
with open('C:/unigis/internet_gis/TAA3/data/ps2019-xygeo.json', encoding='utf-8') as f:
     	data = json.load(f)

# Create empty list for poll station geometries:       
pollstat_list=[]   

# Create empty list for valid votes / geldige stemmen   
vldvts_list=[]

# Create empty lists for votes for each of the 37 political parties
vvd_list=[]
pvda_list=[]
cda_list=[]
sp_list=[]
pvv_list=[]
d66_list=[]
cu_list=[]
gl_list=[]
p50_list=[]
sl_list=[]
fvd_list=[]
pvdd_list=[]
opd_list=[]
denk_list=[]
sgp_list=[]
sebe_list=[]
jl_list=[]
rspct_list=[]
opa_list=[]
fnp_list=[]
pbf_list=[]
nf_list=[]
lpgco_list=[]
gb_list=[]
pvhn_list=[]
ll_list=[]
cusgp_list=[]
lb_list=[]
oahb_list=[]
sebr_list=[]
co_list=[]
pvdo50_list=[]
onh_list=[]
nido_list=[]
u26_list=[]
pvz_list=[]
lpzh_list=[]

# Extract points and data from properties (valid votes and votes for the 37 parties)
for feature in data['features']:
    vldvts = feature['properties']['Geldige stemmen']
    vvd = feature['properties']['VVD']
    pvda = feature['properties']['Partij van de Arbeid (P.v.d.A.)']
    pvda = feature['properties']['Partij van de Arbeid (P.v.d.A.)']
    cda = feature['properties']['CDA']
    sp = feature['properties']['SP (Socialistische Partij)']
    pvv = feature['properties']['PVV (Partij voor de Vrijheid)']
    d66 = feature['properties']['Democraten 66 (D66)']
    cu = feature['properties']['ChristenUnie']
    gl = feature['properties']['GROENLINKS']
    p50 = feature['properties']['50PLUS']
    sl = feature['properties']['Sterk Lokaal']
    fvd = feature['properties']['Forum voor Democratie']
    pvdd = feature['properties']['Partij voor de Dieren']
    opd = feature['properties']['Onafhankelijke Partij Drenthe']
    denk = feature['properties']['DENK']
    sgp = feature['properties']['Staatkundig Gereformeerde Partij (SGP)']
    sebe = feature['properties']['Senioren Belang']
    jl = feature['properties']['JEZUS LEEFT']
    rspct = feature['properties']['Respect!']
    opa = feature['properties']['OPA']
    fnp = feature['properties']['FNP']
    pbf = feature['properties']['Provinciaal Belang Fryslân']
    nf = feature['properties']['Natuurlijk Fryslân']
    lpgco = feature['properties']['Lokale Partijen Gelderland-Code Oranje']
    gb = feature['properties']['Groninger Belang']
    pvhn = feature['properties']['Partij voor het Noorden']
    ll = feature['properties']['LOKAAL-LIMBURG']
    cusgp = feature['properties']['ChristenUnie-SGP']
    lb = feature['properties']['Lokaal Brabant']
    oahb = feature['properties']['Ouderen Appèl - Hart voor Brabant']
    sebr = feature['properties']['Senioren Brabant']
    co = feature['properties']['Code Oranje']
    pvdo50 = feature['properties']['50PLUS/PARTIJ VAN DE OUDEREN']
    onh = feature['properties']['Ouderenpartij Noord-Holland']
    nido = feature['properties']['NIDA']
    u26 = feature['properties']['U26 GEMEENTEN']
    pvz = feature['properties']['Partij voor Zeeland (PVZ)']
    lpzh = feature['properties']['Lokale Partijen Zuid Holland (LPZH)']
    
    # Convert points to shapely format
    geom = shape(feature['geometry'])
    
    # Store shapely points in lists
    pollstat_list.append(geom)
    
    # Append the valid votes from each polling station...
    vldvts_list.append(vldvts)   
    
    # ... and the votes for each party 
    vvd_list.append(vvd)    
    pvda_list.append(pvda)
    cda_list.append(cda)
    sp_list.append(sp)
    pvv_list.append(pvv)
    d66_list.append(d66)
    cu_list.append(cu)
    gl_list.append(gl)
    p50_list.append(p50)
    sl_list.append(sl)
    fvd_list.append(fvd)
    pvdd_list.append(pvdd)
    opd_list.append(opd)
    denk_list.append(denk)
    sgp_list.append(sgp)
    sebe_list.append(sebe)
    jl_list.append(jl)
    rspct_list.append(rspct)
    opa_list.append(opa)
    fnp_list.append(fnp)
    pbf_list.append(pbf)
    nf_list.append(nf)
    lpgco_list.append(lpgco)
    gb_list.append(gb)
    pvhn_list.append(pvhn)
    ll_list.append(ll)
    cusgp_list.append(cusgp)
    lb_list.append(lb)
    oahb_list.append(oahb)
    sebr_list.append(sebr)
    co_list.append(co)
    pvdo50_list.append(pvdo50)
    onh_list.append(onh)
    nido_list.append(nido)
    u26_list.append(u26)
    pvz_list.append(pvz)
    lpzh_list.append(lpzh)   

# Add all lists of each corresponding part to a dictionary
results_per_party = {'VVD': vvd_list, 
                     'Partij van de Arbeid (P.v.d.A.)': pvda_list, 
                     'CDA': cda_list,
                     'SP (Socialistische Partij)': sp_list,
                     'PVV (Partij voor de Vrijheid)': pvv_list,
                     'Democraten 66 (D66)': d66_list,
                     'ChristenUnie': cu_list,
                     'GROENLINKS': gl_list,
                     '50PLUS': p50_list,
                     'Sterk Lokaal': sl_list,
                     'Forum voor Democratie': fvd_list,
                     'Partij voor de Dieren': pvdd_list,
                     'Onafhankelijke Partij Drenthe': opd_list,
                     'DENK': denk_list,
                     'Staatkundig Gereformeerde Partij (SGP)': sgp_list,
                     'Senioren Belang': sebe_list,
                     'JEZUS LEEFT': jl_list,
                     'Respect!': rspct_list,
                     'OPA': opa_list,
                     'FNP': fnp_list,
                     'Provinciaal Belang Fryslân': pbf_list,
                     'Natuurlijk Fryslân': nf_list,
                     'Lokale Partijen Gelderland-Code Oranje': lpgco_list,
                     'Groninger Belang': gb_list,
                     'Partij voor het Noorden': pvhn_list,
                     'LOKAAL-LIMBURG': ll_list,
                     'ChristenUnie-SGP': cusgp_list,
                     'Lokaal Brabant': lb_list,
                     'Ouderen Appèl - Hart voor Brabant': oahb_list,
                     'Senioren Brabant': sebr_list,
                     'Code Oranje': co_list,
                     '50PLUS/PARTIJ VAN DE OUDEREN': pvdo50_list,
                     'Ouderenpartij Noord-Holland': onh_list,
                     'NIDA': nido_list,
                     'U26 GEMEENTEN': u26_list,
                     'Partij voor Zeeland (PVZ)': pvz_list,
                     'Lokale Partijen Zuid Holland (LPZH)': lpzh_list}

# Create a list with all the names of the 37 parties
party_list = list(results_per_party.keys())

# Create copy of results_per_party dictionary 
results_per_party_clear = deepcopy(results_per_party)

# Delete the contents of the list for usage later on
for key in results_per_party_clear:
    results_per_party_clear[key].clear()

    
# PART 3 
# Calculate election results for the four administrative areas 
# and store them in four GeoJSON files

# Election Results Neighbourhoods

# Create 3 lists to store the outcomes for each neighbourhood
results_per_nbhd = []
polls_per_nbhd = []
vldvts_per_nbhd = []
index_nbhd_list = []

# Count all administrative units
count = 0

# Iterate over all neighbourhoods
for nbhd in neighbourhood_list:
    # Create copy of dictionary results_per_party without items in the lists   
    # to store the temporary results
    current_results = deepcopy(results_per_party_clear)    
    # Create lists for temporary storage of the polls and 
    # valid votes per administrative unit
    current_polls = []
    current_vldvts = []
    # Find out which poll stations lie in each administrative unit
    for poll_index, (poll, vldvts) in enumerate(zip(pollstat_list, vldvts_list)):
        # If a poll station lies in a specific administrative unit
        if poll.within(nbhd):
            # Gather all polls
            current_polls.append(poll)
            # Gather all valid votes 
            current_vldvts.append(vldvts)  
            # Find all the votes for each party
            for party in party_list:
                # Gather all votes per party, in case of "None" use "0"
                current_results[party].append(int(results_per_party[party][poll_index] or 0)) 
    # Create count for index number  
    count += 1

    # Append results for each administrative unit
    results_per_nbhd.append(current_results)
    polls_per_nbhd.append(current_polls)         
    vldvts_per_nbhd.append(current_vldvts)
    index_nbhd_list.append(count)

# Create a temporary list to store the features
feature_list = []

# Store neighbourhood data in GeoJSON

# Loop over the names, index, polls, valid votes and results of each the administrative uni
for nbhd_name, nbhd_index, nbhd_shape, nbhd_polls, nbhd_vldvts, nbhd_result in zip(nbhd_names, index_nbhd_list, neighbourhood_list, polls_per_nbhd, vldvts_per_nbhd, results_per_nbhd):
    
    # Create dictionary to store the names of the parties
    # and the percentages of votes per administrative unit
    part_prct_comb = {}  
    
    # Loop over the result list and calculate the percentage of total votes
    # for each of the 37 political parties
    for key, values in nbhd_result.items(): 
        # Make sure only the neighbourhoods are processed where there are 
        # valid votes for a neighbourhood (since this is not always the case)    
        if sum(nbhd_vldvts) > 0: 
        
            # Store each party / percentage combination in a dictionary
            party_prct = {key: round((sum(values)/sum(nbhd_vldvts))*100,2)}
        
            # Append this to the part_perc_comb dict to 
            part_prct_comb.update(party_prct)      
    
    # Create a dictionary to hold the "properties" of the future GeoJSON file
    # and add the id, name of the neighbourhood and the number of polls for each neighbourhood
    properties={'id':nbhd_index,'name':nbhd_name,'polls':len(nbhd_polls)}
    
    # Add the dictionary with the results for each party created above
    properties.update(part_prct_comb)
    
    # Transform the polygon in a GeoJSON format and store all data in a dictionary
    # The first part is the shape, the second the id, the third the name, the 
    # fourth the number of polls within the administrative unit, the fifth the  
    # 37 parties and percentages            
    feature = {'type':'Feature','geometry':shapely.geometry.mapping(nbhd_shape),
                'properties': properties}
                                                                                    
    # Append all features to feature_list
    feature_list.append(feature)
      
# Combine the features in the list in one Feature Collection
feature_collection = FeatureCollection(feature_list)

# Store the feature collection in a GeoJSON file
with open("Neighbourhood_Election_2019.json", "w", encoding='utf-8') as g:
    geojson.dump(feature_collection, g, indent=2)    
    
# Election Results for Districts

# Create 3 lists to store the outcomes for each district
results_per_dist = []
polls_per_dist = []
vldvts_per_dist = []
index_dist_list = []

# Count all administrative units
count = 0

# Iterate over all districts
for dist in district_list:
    # Create copy of dictionary results_per_party without items in the lists   
    # to store the temporary results
    current_results = deepcopy(results_per_party_clear)
    
    # Create lists for temporary storage of the polls and 
    # valid votes per administrative unit
    current_polls = []
    current_vldvts = []
    # Find out which poll stations lie in each administrative unit
    for poll_index, (poll, vldvts) in enumerate(zip(pollstat_list, vldvts_list)):
        # If a poll station lies in a specific administrative unit
        if poll.within(dist):
            # Gather all polls
            current_polls.append(poll)
            # Gather all valid votes 
            current_vldvts.append(vldvts)  
            # Find all the votes for each party
            for party in party_list:
                # Gather all votes per party, in case of "None" use "0"
                current_results[party].append(int(results_per_party[party][poll_index] or 0)) 
    # Create count for index number
    count += 1

    # Append results for each administrative unit
    results_per_dist.append(current_results)
    polls_per_dist.append(current_polls)         
    vldvts_per_dist.append(current_vldvts)
    index_dist_list.append(count)

# Create a temporary list to store the features
feature_list = []

# Store district data in GeoJSON

# Loop over the names, index, polls, valid votes and results of each the administrative uni
for dist_name, dist_index, dist_shape, dist_polls, dist_vldvts, dist_result in zip(dist_names, index_dist_list, district_list, polls_per_dist, vldvts_per_dist, results_per_dist):
    
    # Create dictionary to store the names of the parties
    # and the percentages of votes per administrative unit
    part_prct_comb = {}  
    
    # Loop over the result list and calculate the percentage of total votes
    # for each of the 37 political parties
    for key, values in dist_result.items():
      # Make sure only the districts are processed where there are 
        # valid votes for a district (since this is not always the case)  
        if sum(dist_vldvts) > 0:
        
            # Store each party / percentage combination in a dictionary 
            party_prct = {key: round((sum(values)/sum(dist_vldvts))*100,2)}
        
            # Append this to the part_perc_comb dict to 
            part_prct_comb.update(party_prct)      
   
    # Create a dictionary to hold the "properties" of the future GeoJSON file
    # and add the id, name of the district and the number of polls for each district
    properties={'id':dist_index,'name':dist_name,'polls':len(dist_polls)}
    
    # Add the dictionary with the results for each party created above
    properties.update(part_prct_comb)
    
    # Transform the polygon in a GeoJSON format and store all data in a dictionary
    # The first part is the shape, the second the id, the third the name, the 
    # fourth the number of polls within the administrative unit, the fifth the  
    # 37 parties and percentages    
    feature = {'type':'Feature','geometry':shapely.geometry.mapping(dist_shape),
                'properties': properties}
                                                                                             
    # Append all features to feature_list
    feature_list.append(feature)
      
# Combine the features in the list in one Feature Collection
feature_collection = FeatureCollection(feature_list)

# Store the feature collection in a GeoJSON file
with open("District_Election_2019.json", "w", encoding='utf-8') as g:
    geojson.dump(feature_collection, g, indent=2)


# Election Results for Municipalities

# Create 4 lists to store the results for each municipality
results_per_mncp = []
polls_per_mncp = []
vldvts_per_mncp = []
index_mncp_list = []

# Count all administrative units
count = 0

# Iterate over all municipalities
for mncp in municipality_list:
    # Create copy of dictionary results_per_party without items in the lists   
    # to store the temporary results
    current_results = deepcopy(results_per_party_clear)
    
    # Create lists for temporary storage of the polls and 
    # valid votes per administrative unit
    current_polls = []
    current_vldvts = []
    # Find out which poll stations lie in each administrative unit
    for poll_index, (poll, vldvts) in enumerate(zip(pollstat_list, vldvts_list)):
        # If a poll station lies in a specific administrative unit
        if poll.within(mncp):
            # Gather all polls
            current_polls.append(poll)
            # Gather all valid votes 
            current_vldvts.append(vldvts)  
            # Find all the votes for each party
            for party in party_list:
                # Gather all votes per party, in case of "None" use "0"
                current_results[party].append(int(results_per_party[party][poll_index] or 0)) 
    # Create count for index number
    count += 1

    # Append results for each administrative unit
    results_per_mncp.append(current_results)
    polls_per_mncp.append(current_polls)         
    vldvts_per_mncp.append(current_vldvts)
    index_mncp_list.append(count)
       

# Store municipality data in GeoJSON

# Create a temporary list to store the features
feature_list = []

# Loop over the names, index, polls, valid votes and results of each the administrative uni
for mncp_name, mncp_index, mncp_shape, mncp_polls, mncp_vldvts, mncp_result in zip(mncp_names, index_mncp_list, municipality_list, polls_per_mncp, vldvts_per_mncp, results_per_mncp):
            
    # Create dictionary to store the names of the parties
    # and the percentages of votes per administrative unit
    part_prct_comb = {}  
    
    # Loop over the result list and calculate the percentage of total votes
    # for each of the 37 political parties 
    for key, values in mncp_result.items():
        # Make sure only the municipalities are processed where there are 
        # valid votes for a municipality (since this is not always the case)  
        if sum(mncp_vldvts) > 0:
        
            # Store each party / percentage combination in a dictionary  
            party_prct = {key: round((sum(values)/sum(mncp_vldvts))*100,2)}
        
            # Append this to the part_perc_comb dict to 
            part_prct_comb.update(party_prct)      
   
    # Create a dictionary to hold the "properties" of the future GeoJSON file
    # and add the id, name of the municipality and the number of polls for each municipality
    properties={'id':mncp_index,'name':mncp_name,'polls':len(mncp_polls)}
    
    # Add the dictionary with the results for each party created above
    properties.update(part_prct_comb)
    
    # Transform the polygon in a GeoJSON format and store all data in a dictionary
    # The first part is the shape, the second the id, the third the name, the 
    # fourth the number of polls within the administrative unit, the fifth the  
    # 37 parties and percentages
    feature = {'type':'Feature','geometry':shapely.geometry.mapping(mncp_shape),
                'properties': properties}

    # Append all features to feature_list
    feature_list.append(feature)                                                                                        
        
# Combine the features in the list in one Feature Collection
feature_collection = FeatureCollection(feature_list)

# Store the feature collection in a GeoJSON file
with open("Municipality_Election_2019.json", "w", encoding='utf-8') as g:
    geojson.dump(feature_collection, g, indent=2)


# Election Results for Provinces

# Create 4 lists to store the results, polls, 
# valid votes and index for each province
results_per_prov = []
polls_per_prov = []
vldvts_per_prov = []
index_prov_list = []

# Count all administrative units
count = 0

# Iterate over all provinces
for prov in province_list:
    # Create copy of dictionary results_per_party without items in the lists   
    # to store the temporary results
    current_results = deepcopy(results_per_party_clear)
    
    # Create lists for temporary storage of the polls and 
    # valid votes per administrative unit
    current_polls = []
    current_vldvts = []
    
    # Find out which poll stations lie in each administrative unit
    for poll_index, (poll, vldvts) in enumerate(zip(pollstat_list, vldvts_list)):
        # If a poll station lies in a specific administrative unit
        if poll.within(prov):
            # Gather all polls
            current_polls.append(poll)
            # Gather all valid votes 
            current_vldvts.append(vldvts)  
            # Find all the votes for each party
            for party in party_list:
                # Gather all votes per party, in case of "None" use "0"
                current_results[party].append(int(results_per_party[party][poll_index] or 0)) 
    # Create count for index number
    count += 1              
    # Append results, polls, valid votes and index for each administrative unit
    results_per_prov.append(current_results)
    polls_per_prov.append(current_polls)         
    vldvts_per_prov.append(current_vldvts) 
    index_prov_list.append(count)

# Create a temporary list to store the features
feature_list = []

# Store province data in GeoJSON

# Loop over the names, index, polls, valid votes and results of each the administrative uni
for prov_name, prov_index, prov_shape, prov_polls, prov_vldvts, prov_result in zip(prov_names, index_prov_list, province_list, polls_per_prov, vldvts_per_prov, results_per_prov):
    
    # Create dictionary to store the names of the parties
    # and the percentages of votes per administrative unit TH: hier moet het anders
    part_prct_comb = {}  
    
    # Loop over the result list and calculate the percentage of total votes
    # for each of the 37 political parties
    for key, values in prov_result.items():
        
        # Store each party / percentage combination in a dictionary 
        party_prct = {key: round((sum(values)/sum(prov_vldvts))*100,2)} #allemaal losse dicts
                
        # Append this to the part_perc_comb dict to 
        part_prct_comb.update(party_prct)   
      
    # Create a dictionary to hold the "properties" of the future GeoJSON file
    # and add the id, name of the province and the number of polls for each province
    properties={'id':prov_index,'name':prov_name,'polls':len(prov_polls)}
    
    # Add the dictionary with the results for each party created above
    properties.update(part_prct_comb)
    
   
    # Transform the polygon in a GeoJSON format and store all data in a dictionary
    # The first part is the shape, the second the id, the third the name, the 
    # fourth the number of polls within the administrative unit, the fifth the  
    # 37 parties and percentages       
    feature = {'type':'Feature','geometry':shapely.geometry.mapping(prov_shape),
               'properties': properties}

    # Append all features to feature_list
    feature_list.append(feature)
      
# Combine the features in the list in one Feature Collection
feature_collection = FeatureCollection(feature_list)

# Store the feature collection in a GeoJSON file
with open("Province_Election_2019.json", "w", encoding='utf-8') as g:
    geojson.dump(feature_collection, g, indent=2)
    
